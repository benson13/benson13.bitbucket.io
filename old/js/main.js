( function($) {

'use strict';

// background video
  window.addEventListener('load', function () {
    var video = document.querySelector('#video');
    var preloader = document.querySelector('.preloader');

    function checkLoad() {
      if (video.readyState === 4) {
        $(preloader).fadeOut();
        $(video).fadeIn();
        video.play();
        
      } else {
        setTimeout(checkLoad, 4000);
      }
    }
    setTimeout( function() {
      checkLoad();
    }, 4000)
  }, true);

  $(document).ready(function () {

    // popup youtube video
    $('.popup-youtube').fancybox();

    // $('.carousel-image').slick({
    //   asNavFor: '.carousel-info',
    //   autoplay: true,
    //   autoplaySpeed: 3000,
    //   infinite: false,
    //   slidesToShow: 4,
    //   slidesToScroll: 1,
    //   dots: false,
    //   fade: true,
    // });
    
     $('.carousel-info').slick({
      // asNavFor: '.carousel-image',
      autoplay: true,
      autoplaySpeed: 3000,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      pauseOnHover: false,
    });

    // highlight carousel image
    function highlight(items, index) {
      index = index % items.length;
      items.removeClass('active');
      items.eq(index).addClass('active');
      setTimeout(function () { highlight(items, index + 1) }, 3500);
    }

    highlight($(".carousel-image .image-holder"), 0)

  });

  $(window).resize( function() {
    console.log($(window).width());
  })


    // Init ScrollMagic
    var controller = new ScrollMagic.Controller();

      	// change behaviour of controller to animate scroll instead of jump
	controller.scrollTo(function (newpos) {
		TweenMax.to(window, 0.5, {scrollTo: {y: newpos}});
	});

  //  Bind scroll to anchor links
    $(document).on("click", ".pin-navigation a", function(e) {
      var id = $(this).attr("href");

      if($(id).length > 0) {
        e.preventDefault();

        // trigger scroll
        controller.scrollTo(id);

        // If supported by the browser we can also update the URL
        if (window.history && window.history.pushState) {
          history.pushState("", document.title, id);
        }
      }

    });

    // pin the intro
    var mainTitle = new TimelineMax();
    mainTitle
      .from('.main-title', 0.2, { y: '0%', autoAlpha: 1, ease: Power0.easeNone }, 0)
      .to('.main-title', 1, { y: '-420', autoAlpha: 1, ease: Power0.easeNone }, 0.2);


    var pinIntroScene1 = new ScrollMagic.Scene({
      triggerElement: ".item-1 .pin-wrapper",
      triggerHook: 0,
      duration: "100%"
    })

      .setPin(".item-1 .pin-wrapper", { pushFollowers: false })
      // .setClassToggle("#item-1", "active") // add class toggle
      .setTween(mainTitle)
      .addIndicators()
      .addTo(controller);

    // pin 2
    var pinIntroScene2 = new ScrollMagic.Scene({
      triggerElement: ".item-2 .pin-wrapper",
      triggerHook: 1
    })

      .setPin(".item-2 .pin-wrapper", { pushFollowers: false })
      // .setClassToggle("#item-2", "active") // add class toggle
      // .setTween(introText)
      .addIndicators()
      .addTo(controller);
    // pin 3
    var pinIntroScene3 = new ScrollMagic.Scene({
      triggerElement: ".item-3 .pin-wrapper",
      triggerHook: 1
    })

      .setPin(".item-3 .pin-wrapper", { pushFollowers: false })
      // .setClassToggle("#item-3", "active") // add class toggle
      // .addIndicators({ name: "1 (duration: 300)" })
      // .setTween(introText)
      .addIndicators()
      .addTo(controller);

    // pin 4
    var pinIntroScene4 = new ScrollMagic.Scene({
      triggerElement: ".item-4 .pin-wrapper",
      triggerHook: 1
    })

      .setPin(".item-4 .pin-wrapper", { pushFollowers: false })
      // .setClassToggle("#item-4", "active") // add class toggle
      // .addIndicators({ name: "1 (duration: 300)" })
      // .setTween(introText)
      .addIndicators()
      .addTo(controller);

    // pin 5
    var pinIntroScene5 = new ScrollMagic.Scene({
      triggerElement: ".item-5 .pin-wrapper",
      triggerHook: 1,
      duration: "100%"
    })

      .setPin(".item-5 .pin-wrapper", { pushFollowers: false })
      // .setClassToggle("#item-5", "active") // add class toggle
      // .addIndicators({ name: "1 (duration: 100%)" })
      .addIndicators()
      // .setTween(introText)
      .addTo(controller);


    // Call to action
    $('.phone-code').change(function() {
      // $(this).find('option:selected').remove();
      $('#temp-opt').html($('option:selected', this).text());
      $(this).width($('#temp-select').width());
      console.log($(this).width())
      console.log($('.phone-code option:selected').text());
    });

    let button0Clicked = false;
    let button1Clicked = false;

    function sendSmsClicked(num) {
      let selectElem = document.getElementById("phoneCode" + num);
      let textInputField = document.getElementById("number" + num);
      let errorElem = document.getElementById("error" + num);
      let elem = document.getElementById("button" + num + "text");
      let countryCode = selectElem.value;
      let number = textInputField.value;
      if (number === "") {
        return;
      }
      if (button0Clicked && num === 0) {
        return;
      }
      if (button1Clicked && num === 1) {
        return;
      }
      if (num === 0) {
        button0Clicked = true;
      } else {
        button1Clicked = true;
      }
      // analytics.track("linktexting_" + (num === 0 ? "top" : "bottom"));
      console.log(errorElem)
      // errorElem.text('Placeholder for errors');
      if (num === 0) {
        $(elem).html('sms sent');
      } else {
        $(elem).html('sms not sent');
      }
      number = "+" + countryCode + number;
      fetch("http://api-dev-abq12ckl5tmbhqq.qually.com:45338/0/sendapplink",
        {
          method: "POST",
          body: JSON.stringify({
            mobileNumber: number
          }),
          headers: {
            "Content-Type": "application/json",
            "api-version": "0"
          }
        }
      )
        .then(function(response) {
          return response.text();
        })
        .then(function(json) {
          let jsonObj = JSON.parse(json);
          let mobileNumberValid = jsonObj.mobileNumberValid;
          let sentTooManyTimes = jsonObj.sentTooManyTimes;
          if (mobileNumberValid === false) {
            $(errorElem).text("Please enter a valid mobile number").fadeIn();
            // $(errorElem).style.color = num === 0 ? "#ffffff" : "#4586de";
            $(elem).html("Text me the link");
            if (num === 0) {
              button0Clicked = false;
            } else {
              button1Clicked = false;
            }
            return;
          }
          if (sentTooManyTimes === true) {
            $(errorElem).text("You have requested a link too many times. Please use the below buttons instead").faeIn();
            // $(errorElem).style.color = num === 0 ? "#ffffff" : "#4586de";
            $(elem).html("Text me the link");
            if (num === 0) {
              button0Clicked = false;
            } else {
              button1Clicked = false;
            }
            return;
          }
        })
        .catch(function(err) {
          $(errorElem).html('Something went wrong. Please try again').show();
          // $(errorElem).style.color = num === 0 ? "#ffffff" : "#4586de";
          $(elem).html("Text me the link");
          if (num === 0) {
            button0Clicked = false;
          } else {
            button1Clicked = false;
          }
        });
    }

    $('.text-me-btn').on('click', function() {
      sendSmsClicked(1)
    })

    $('.type-text').each(function(index) {
      var textWidth = $(this).width();
      var text = $('.type-text').text();
      console.log(text(index))

      $(this).css('width', textWidth);
    })

})(jQuery)

