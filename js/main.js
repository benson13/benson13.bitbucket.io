( function($) {

'use strict';

// background video
  window.addEventListener('load', function () {
    var video = document.querySelector('#video');
    var preloader = document.querySelector('.preloader');

    function checkLoad() {
      if (video.readyState === 4) {
        $(preloader).hide();
        $(video).show();
        video.play();
        
      } else {
        setTimeout(checkLoad, 3000);
      }
    }
    setTimeout( function() {
      checkLoad();
    }, 3000)
  }, true);

  $(document).ready(function () {

    // popup youtube video
    $('.popup-youtube').fancybox();
    
    //  $('.carousel-info').slick({
    //   autoplay: true,
    //   autoplaySpeed: 3000,
    //   infinite: true,
    //   slidesToShow: 1,
    //   slidesToScroll: 1,
    //   arrows: false,
    //   fade: true,
    //   pauseOnHover: false,
    // });

    // $('.active-img').each(function() {
    //   var imgsrc = $(this).attr('src');
    //   // console.log(imgsrc)
    //   // $(".active-img").attr("src", "");

    //   if ($(".image-holder").not("active")) {
    //     // console.log('.active-img', this)
    //     console.log(this)
    //     $('.active-img', this).attr("src", imgsrc);
    //   } else {
    //     console.log(".active-img", this);
    //     $(".active-img", this).attr("src", '');
    //   }
    // })

    
    $('.pin-navigation .dots').bind('click', function (e) {
      e.stopPropagation(); // prevent hard jump, the default behavior

      var target = $(this).attr("href"); // Set the target as variable

      // perform animated scrolling by getting top-position of target-element and set it as scroll target
      $('html, body').animate({
        scrollTop: $(target).offset().top
      }, 600, function () {
        location.hash = target; //attach the hash (#jumptarget) to the pageurl
      });

      return false;
    });

    // function scrollNav() {
    //   $('.pin-navigation a').click(function () {
    //     e.preventDefault();
    //     //Toggle Class
    //     // $(".active").removeClass("active");
    //     $(this).addClass("active");
    //     var theClass = $(this).attr("class");
    //     $('.' + theClass).addClass('active');
    //     //Animate
    //     $('html, body').stop().animate({
    //       scrollTop: $($(this).attr('href')).offset().top
    //     }, 600, function () {
    //         location.hash = target;
    //     });
    //     return false;
    //   });
    //   // $('.scrollTop a').scrollTop();
    // }
    // scrollNav();

    $(window).scroll(function () {
      var scrollDistance = $(window).scrollTop();
      // console.log(scrollDistance)
      var scrollTop = $('#section-4').offset().top;
      // console.log(scrollTop)

      // Assign active class to nav links while scolling
      $('.item').each(function (i) {
        console.log($(this).offset().top)
        if ($(this).offset().top <= scrollDistance) {
          $('.pin-navigation a.active').removeClass('active');
          $('.pin-navigation a').eq(i).addClass('active');
        }
      });
    }).scroll();
   
    // highlight carousel image
    function highlight(items, index) {
      index = index % items.length;
      items.removeClass('active');
      items.eq(index).addClass('active');
      setTimeout(function () { highlight(items, index + 1) }, 3500);
    }

    highlight($(".carousel-image .image-holder"), 0)
    highlight($(".carousel-info p"), 0);

    // function restartGif(ImageSelector) {
    //   var imgSrc = document.querySelector(ImageSelector).src;
    //   document.querySelector(ImageSelector).src = imgSrc;
    // }

    // $(".active-img").each(function() {
    //   setInterval(function() {
    //     restartGif(this);
    //   }, 3000);
    // });

  });

  $(window).resize( function() {
    console.log($(window).width());
  })
    

  // Init ScrollMagic
  var controller = new ScrollMagic.Controller();
  
  // pin the intro

  if($(window).width() >= 991 ) {
    var mainTitle = new TimelineMax();
    mainTitle
      .from('.main-title', 0.2, { y: '0', autoAlpha: 1, ease: Power0.easeInOut }, 0)
      .to('.main-title', 1, { y: '-275', autoAlpha: 1, ease: Power0.easeInOut }, 0.2)
      // .from('.item-1', 0.2, { autoAlpha: 0, ease: Power0.easeInOut }, 0)
      // .to('.item-1', 1, { autoAlpha: 1, ease: Power0.easeInOut }, 0.2)
  } else if ($(window).width() < 640) {
    var mainTitle = new TimelineMax();
    mainTitle
      .from('.main-title', 0.2, { y: '0', autoAlpha: 1, ease: Power0.easeInOut }, 0)
      .to('.main-title', 1, { y: '-113', autoAlpha: 1, ease: Power0.easeInOut }, 0.2)
  } else {
    var mainTitle = new TimelineMax();
    mainTitle
      .from('.main-title', 0.2, { y: '0', autoAlpha: 1, ease: Power0.easeInOut }, 0)
      .to('.main-title', 1, { y: '-205', autoAlpha: 1, ease: Power0.easeInOut }, 0.2)
  }
  

  // var fadeIn2 = new TimelineMax();
  // fadeIn2
  //   .from('.item-2', 0.2, { autoAlpha: 0, ease: Power1.easeInOut }, 0)
  //   .to('.item-2', 1, { autoAlpha: 1, ease: Power1.easeInOut }, 0.2)


  // var fadeIn3 = new TimelineMax();
  // fadeIn3
  //   .from('.item-3', 0.2, { autoAlpha: 0, ease: Power1.easeInOut }, 0)
  //   .to('.item-3', 1, { autoAlpha: 1, ease: Power1.easeInOut }, 0.2)


  // var fadeIn4 = new TimelineMax();
  // fadeIn4
  //   .from('.item-4', 0.2, { autoAlpha: 0, ease: Power1.easeInOut }, 0)
  //   .to('.item-4', 1, { autoAlpha: 1, ease: Power1.easeInOut }, 0.2)


  // var fadeIn5 = new TimelineMax();
  // fadeIn5
  //   .from('.item-5', 0.2, { autoAlpha: 0, ease: Power1.easeInOut }, 0)
  //   .to('.item-5', 1, { autoAlpha: 1, ease: Power1.easeInOut }, 0.2)


  // pin navigation
  var pinNavigation = new ScrollMagic.Scene({
    triggerElement: "#section-4",
    triggerHook: 0,
    duration: $('#section-4').height() * 0.75,
  })
  .setPin(".pin-navigation", { pushFollowers: false })
  // .addIndicators({
  //   name: "1 (duration section height)"
  // })
  .addTo(controller);


  var pinIntroScene1 = new ScrollMagic.Scene({
    triggerElement: ".item-1 .pin-wrapper",
    triggerHook: 0,
    // duration: '100%'
  })

    .setPin(".item-1 .pin-wrapper", { pushFollowers: true })
    // .setClassToggle("#pin-1", "active") // add class toggle
    .setTween(mainTitle)
    .addIndicators({ name: "1 (duration: 100%)" })
    .addTo(controller);

  // pin 2
  var pinIntroScene2 = new ScrollMagic.Scene({
    triggerElement: ".item-2 .pin-wrapper",
    triggerHook: 1,
    // duration: "100%"
  })

    .setPin(".item-2 .pin-wrapper", { pushFollowers: false })
    // .setClassToggle("#pin-2", "active") // add class toggle
    // .setTween(secondSection)
    // .addIndicators()
    .addTo(controller);

  // pin 3
  var pinIntroScene3 = new ScrollMagic.Scene({
    triggerElement: ".item-3 .pin-wrapper",
    triggerHook: 1,
    // duration: "100%"
  })

    .setPin(".item-3 .pin-wrapper", { pushFollowers: false })
    // .setClassToggle("#pin-3", "active") // add class toggle
    // .addIndicators({ name: "1 (duration: 300)" })
    // .setTween(fadeIn3)
    // .addIndicators()
    .addTo(controller);

  // pin 4
  var pinIntroScene4 = new ScrollMagic.Scene({
    triggerElement: ".item-4 .pin-wrapper",
    triggerHook: 1,
    // duration: "100%"
  })

    .setPin(".item-4 .pin-wrapper", { pushFollowers: false })
    // .setClassToggle("#pin-4", "active") // add class toggle
    // .addIndicators({ name: "1 (duration: 300)" })
    // .setTween(fadeIn4)
    // .addIndicators()
    .addTo(controller);

  // pin 5
  var pinIntroScene5 = new ScrollMagic.Scene({
    triggerElement: ".item-5 .pin-wrapper",
    triggerHook: 1,
    duration: "100%"
  })

    .setPin(".item-5 .pin-wrapper", { pushFollowers: true })
    .setClassToggle("#pin-5", "active") // add class toggle
    // .addIndicators({ name: "1 (duration: 100%)" })
    // .addIndicators()
    // .setTween(fadeIn5)
    .addTo(controller);

  // function scrollNav() {
  //   $('.pin-navigation a').click(function () {
  //     //Toggle Class
  //     $(".active").removeClass("active");
  //     $(this).addClass("active");
  //     var theClass = $(this).attr("class");
  //     $('.' + theClass).addClass('active');
  //     //Animate
  //     $('html, body').stop().animate({
  //       scrollTop: $($(this).attr('href')).offset().top - 160
  //     }, 400);
  //     return false;
  //   });
  //   $('.scrollTop a').scrollTop();
  // }
  // scrollNav();


  // Call to action
  $('.phone-code').change(function() {
    // $(this).find('option:selected').remove();
    $('#temp-opt').html($('option:selected', this).text());
    $(this).width($('#temp-select').width());
    console.log($(this).width())
    console.log($('.phone-code option:selected').text());
  });

  let button0Clicked = false;
  let button1Clicked = false;

  function sendSmsClicked(num) {
    let selectElem = document.getElementById("phoneCode" + num);
    let textInputField = document.getElementById("number" + num);
    let errorElem = document.getElementById("error" + num);
    let elem = document.getElementById("button" + num + "text");
    let countryCode = selectElem.value;
    let number = textInputField.value;
    if (number === "") {
      return;
    }
    if (button0Clicked && num === 0) {
      return;
    }
    if (button1Clicked && num === 1) {
      return;
    }
    if (num === 0) {
      button0Clicked = true;
    } else {
      button1Clicked = true;
    }
    // analytics.track("linktexting_" + (num === 0 ? "top" : "bottom"));
    console.log(errorElem)
    // errorElem.text('Placeholder for errors');
    if (num === 0) {
      $(elem).html('sms sent');
    } else {
      $(elem).html('sms not sent');
    }
    number = "+" + countryCode + number;
    fetch("http://api-dev-abq12ckl5tmbhqq.qually.com:45338/0/sendapplink",
      {
        method: "POST",
        body: JSON.stringify({
          mobileNumber: number
        }),
        headers: {
          "Content-Type": "application/json",
          "api-version": "0"
        }
      }
    )
      .then(function(response) {
        return response.text();
      })
      .then(function(json) {
        let jsonObj = JSON.parse(json);
        let mobileNumberValid = jsonObj.mobileNumberValid;
        let sentTooManyTimes = jsonObj.sentTooManyTimes;
        if (mobileNumberValid === false) {
          $(errorElem).text("Please enter a valid mobile number").fadeIn();
          // $(errorElem).style.color = num === 0 ? "#ffffff" : "#4586de";
          $(elem).html("Text me the link");
          if (num === 0) {
            button0Clicked = false;
          } else {
            button1Clicked = false;
          }
          return;
        }
        if (sentTooManyTimes === true) {
          $(errorElem).text("You have requested a link too many times. Please use the below buttons instead").faeIn();
          // $(errorElem).style.color = num === 0 ? "#ffffff" : "#4586de";
          $(elem).html("Text me the link");
          if (num === 0) {
            button0Clicked = false;
          } else {
            button1Clicked = false;
          }
          return;
        }
      })
      .catch(function(err) {
        $(errorElem).html('Something went wrong. Please try again').show();
        // $(errorElem).style.color = num === 0 ? "#ffffff" : "#4586de";
        $(elem).html("Text me the link");
        if (num === 0) {
          button0Clicked = false;
        } else {
          button1Clicked = false;
        }
      });
  }

  $('.text-me-btn').on('click', function() {
    sendSmsClicked(1)
    sendSmsClicked(2);
  })





})(jQuery)

